<?php

use Slim\Http\{Request, Response};

// Routes
$app->get('/', function (Request $request, Response $response) {
    return $response->withJson([
        'status' => false,
        'message' => 'Do donut for you, buddy.',
    ]);
});

$app->get('/vehicles', function (Request $request, Response $response) {
    $results = [
        'Count' => 0,
        'Results' => [],
    ];
    $body = $request->getParsedBody();

    if (empty($body)) {
        return $response->withJson($results);
    }

    if (!isset($body['modelYear'], $body['manufacturer'], $body['model'])) {
        return $response->withJson($results);
    }

    $year = $body['modelYear'];
    $manufacturer = $body['manufacturer'];
    $model = $body['model'];
    $withRating = $request->getParam('withRating');

    $resp = $this->NHTSAService->getModelYearData($year, $manufacturer, $model);

    if ('' === $resp) {
        return $response->withJson($results);
    }

    $results = $this->NHTSAService->parseResponse($resp, 'true' === $withRating);

    return $response->withJson($results);
});

$app->get('/vehicles/{year}/{manufacturer}/{model}', function (Request $request, Response $response, array $args) {
    $results = [
        'Count' => 0,
        'Results' => [],
    ];
    $year = $args['year'];
    $manufacturer = $args['manufacturer'];
    $model = $args['model'];
    $withRating = $request->getParam('withRating');

    $resp = $this->NHTSAService->getModelYearData($year, $manufacturer, $model);

    if ('' === $resp) {
        return $response->withJson($results);
    }

    $results = $this->NHTSAService->parseResponse($resp, 'true' === $withRating);

    return $response->withJson($results);
});
