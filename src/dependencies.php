<?php

// DIC configuration
use GuzzleHttp\Client;
use Modus\Services\NHTSAService;

$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];

    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));

    return $logger;
};

// NHTSA Service
$container['NHTSAService'] = function () {
    return new NHTSAService(new Client());
};
