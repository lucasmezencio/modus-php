# Modus Create PHP API Development

This simple API was built on top of Slim Framework.

## Requirements

- PHP 7.2+
- Composer

## Install the Application

Clone this repository and run the following command from the directory where the repository was cloned:

    composer install

To run the application in development, you can run these commands 

    cd [path-to-cloned-repository]
    composer start

Access the API at http://localhost:8080/

That's it!
