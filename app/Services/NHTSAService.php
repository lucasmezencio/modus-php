<?php

namespace Modus\Services;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class NHTSAService
 *
 * @package Modus\Services
 */
class NHTSAService
{
    private const ROOT_URL = 'https://one.nhtsa.gov/webapi/api/SafetyRatings';
    private const MODEL_URL = '/modelyear/:YEAR:/make/:MANUFACTURER:/model/:MODEL:?format=json';
    private const VEHICLE_URL = '/VehicleId/:VEHICLE_ID:?format=json';

    /** @var ClientInterface */
    private $client;

    /**
     * NHTSAService constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    private function makeCall(string $url): string
    {
        try {
            $body = $this->client->request('GET', self::ROOT_URL . $url)->getBody();
            $contents = $body->getContents();
        } catch (GuzzleException $e) {
            $contents = '';
        }

        return $contents;
    }

    /**
     * @param $year
     * @param $manufacturer
     * @param $model
     *
     * @return mixed
     */
    public function getModelYearData($year, $manufacturer, $model)
    {
        $url = str_replace(
            [':YEAR:', ':MANUFACTURER:', ':MODEL:'],
            [$year, $manufacturer, $model],
            self::MODEL_URL
        );

        return \GuzzleHttp\json_decode($this->makeCall($url));
    }

    /**
     * @param $vehicleId
     *
     * @return mixed
     */
    private function getVehicleData($vehicleId)
    {
        $url = str_replace(':VEHICLE_ID:', $vehicleId, self::VEHICLE_URL);

        return \GuzzleHttp\json_decode($this->makeCall($url));
    }

    /**
     * @param object $response
     * @param bool $withRating
     *
     * @return array
     */
    public function parseResponse($response, bool $withRating = false): array
    {
        $results = [
            'Count' => $response->Count,
        ];

        foreach ($response->Results as $result) {
            $vehicle = [
                'Description' => $result->VehicleDescription,
                'VehicleId' => $result->VehicleId,
            ];

            if ($withRating) {
                $resp = $this->getVehicleData($result->VehicleId);

                if ('' !== $resp) {
                    $vehicle['CrashRating'] = $resp->Results[0]->OverallRating;
                }
            }

            $results['Results'][] = $vehicle;
        }

        return $results;
    }
}
